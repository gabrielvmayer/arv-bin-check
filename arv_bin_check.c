#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
    int info;
    struct arvore *esq;
    struct arvore *dir;
} Arvore;


Arvore*  cria_arv_vazia (void);
Arvore*  arv_constroi (int c, Arvore* e, Arvore* d);
int      verifica_arv_vazia (Arvore* a);
Arvore*  arv_libera (Arvore* a);
void     arv_imprime (Arvore* a);

/**/
int arv_bin_check(Arvore *a, Arvore *min, Arvore *max);
/**/

Arvore* cria_arv_vazia (void) {
    return NULL;
}

Arvore* arv_constroi (int c, Arvore* e, Arvore* d) {
    Arvore* a = (Arvore*)malloc(sizeof(Arvore));
    a->info = c;
    a->esq = e;
    a->dir = d;
    return a;
}

int verifica_arv_vazia (Arvore* a) {
    return (a == NULL);
}

Arvore* arv_libera (Arvore* a) {
    if (!verifica_arv_vazia(a)) {
        arv_libera (a->esq);
        arv_libera (a->dir);
        free(a);
    }
    return NULL;
}

void arv_imprime (Arvore* a) {
	if(!a) return;

	arv_imprime(a->esq);
    printf("%d ", a->info);
	arv_imprime(a->dir);
}

/**/
int arv_bin_check(Arvore *a, Arvore *min, Arvore *max) {
    if (a == NULL)
        return 1;

    if ((min != NULL && a->info <= min->info) || (max != NULL && a->info >= max->info))
        return 0;

    return arv_bin_check(a->esq, min, a) && arv_bin_check(a->dir, a, max);
}
/**/

int main () {
    Arvore *arvBinaria          = arv_constroi(8, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->esq             = arv_constroi(4, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->esq->esq        = arv_constroi(2, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->esq->esq->esq   = arv_constroi(1, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->esq->esq->dir   = arv_constroi(3, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->esq->dir        = arv_constroi(6, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->esq->dir->esq   = arv_constroi(5, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->esq->dir->dir   = arv_constroi(7, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->dir             = arv_constroi(12, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->dir->esq        = arv_constroi(10, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->dir->esq->esq   = arv_constroi(9, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->dir->esq->dir   = arv_constroi(11, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->dir->dir        = arv_constroi(14, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->dir->dir->esq   = arv_constroi(13, cria_arv_vazia(), cria_arv_vazia());
    arvBinaria->dir->dir->dir   = arv_constroi(15, cria_arv_vazia(), cria_arv_vazia());

    arv_imprime(arvBinaria);
    (arv_bin_check(arvBinaria, NULL, NULL) == 1) ? printf("\nArvore acima eh binaria\n\n") : printf("\nArvore acima nao eh binaria\n\n");

    Arvore *arvNaoBinaria          = arv_constroi(14, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->esq             = arv_constroi(4, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->esq->esq        = arv_constroi(12, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->esq->esq->esq   = arv_constroi(3, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->esq->esq->dir   = arv_constroi(2, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->esq->dir        = arv_constroi(15, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->esq->dir->esq   = arv_constroi(5, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->esq->dir->dir   = arv_constroi(7, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->dir             = arv_constroi(13, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->dir->esq        = arv_constroi(10, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->dir->esq->esq   = arv_constroi(9, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->dir->esq->dir   = arv_constroi(1, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->dir->dir        = arv_constroi(11, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->dir->dir->esq   = arv_constroi(8, cria_arv_vazia(), cria_arv_vazia());
    arvNaoBinaria->dir->dir->dir   = arv_constroi(6, cria_arv_vazia(), cria_arv_vazia());
    arv_imprime(arvNaoBinaria);
    (arv_bin_check(arvNaoBinaria, NULL, NULL) == 1) ? printf("\nArvore acima eh binaria\n") : printf("\nArvore acima nao eh binaria\n");

    return 0;
}